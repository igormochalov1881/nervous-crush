﻿using UnityEngine;
using System.Collections;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _bulletPrefab;

    private float _bulletForce = -10f;
    private Transform _startPosition;

    private void Start()
    {
        _startPosition = gameObject.transform;
        StartCoroutine("Shoot");
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            GameObject _bullet = Instantiate(_bulletPrefab, _startPosition.position, Quaternion.identity);
            _bullet.AddComponent<Bullet>();
            Rigidbody2D _rigidBody = _bullet.GetComponent<Rigidbody2D>();
            _rigidBody.velocity = new Vector3(_bulletForce, 0, 0);
            yield return new WaitForSeconds(2f);
        }
    }
}