﻿using System.Collections;
using UnityEngine;

public class DynamicTimerPlatform : MonoBehaviour
{
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRenderer;
    private bool _isVisible = true;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine("WaitForTwoSeconds");
    }

    private IEnumerator WaitForTwoSeconds()
    {
        while (true)
        {
            _isVisible = !_isVisible;
            yield return new WaitForSeconds(2);
            _collider.enabled = _isVisible;
            if (!_isVisible)
                _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 0.25f);
            else
                _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 1f);
        }
    }
}
