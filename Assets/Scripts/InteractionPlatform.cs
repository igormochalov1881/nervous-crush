﻿using System.Collections;
using UnityEngine;

public class InteractionPlatform : MonoBehaviour
{
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine("MakeInvisible");
    }

    private IEnumerator MakeInvisible()
    {
        yield return new WaitForSeconds(1);
        _collider.enabled = false;
        _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 0.25f);
        yield return new WaitForSeconds(2);
        _collider.enabled = true;
        _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 1f);
    }
}