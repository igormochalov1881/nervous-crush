﻿using UnityEngine;

public class Lift : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _animator.SetBool("IsTriggered", true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _animator.SetBool("IsTriggered", false);
    }
}
