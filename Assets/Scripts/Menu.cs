﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Continue()
    {
        FindObjectOfType<AudioManager>().Play("ButtonSound");
        gameObject.SetActive(false);
    }
    
    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitToMainMenu()
    {
        FindObjectOfType<AudioManager>().Play("ButtonSound");
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}