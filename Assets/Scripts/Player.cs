﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private CharacterController2D _controller;
    [SerializeField] private float _runSpeed = 5f;
    [SerializeField] private GameObject _finish;
    [SerializeField] private Vector3 _finishPosition;
    [SerializeField] private Animator _transition;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _menu;

    private bool _isJumping;
    private bool _isMenuVisible = false;
    private float _moveHorizontal;
    public int Health { get; set; } = 3;
    private Vector3 _safePositionHumanWorld;

    private void Start()
    {
        FindObjectOfType<AudioManager>().Play("BackgroundMelody");
        _menu.SetActive(_isMenuVisible);
        _finishPosition = _finish.transform.position;
    }

    private void Update()
    {
        _moveHorizontal = Input.GetAxisRaw("Horizontal");

        _animator.SetFloat("Speed", Mathf.Abs(_moveHorizontal));

        if (Input.GetKeyDown(KeyCode.Space))
        {
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            _isJumping = true;
            _animator.SetBool("IsJumping", true);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_menu.activeSelf)
            {
                _isMenuVisible = false;
                _menu.SetActive(_isMenuVisible);
            }
            else
            {
                _isMenuVisible = true;
                _menu.SetActive(_isMenuVisible);
            }
        }
    }

    public void OnLanding()
    {
        _animator.SetBool("IsJumping", false);
    }

    private void FixedUpdate()
    {
        _controller.Move(_moveHorizontal * _runSpeed * Time.deltaTime, false, _isJumping);
        _isJumping = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Trap")
        {
            FindObjectOfType<AudioManager>().Play("DeathSound");
            gameObject.transform.position = _safePositionHumanWorld;
        }
        else if (collision.tag == "SafePosition")
        {
            FindObjectOfType<AudioManager>().Play("CheckPointSound");
            _safePositionHumanWorld = collision.transform.position;
            collision.gameObject.SetActive(false);
        } 
        else if (collision.tag == "Finish")
        {
            FindObjectOfType<AudioManager>().Play("FinishSound");
            this.transform.position = _finishPosition;
        }
    }
}