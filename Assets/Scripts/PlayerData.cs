﻿using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int Health;
    public float[] CheckPointPosition;

    /*public PlayerData(Player player)
    {
        Health = player.Health;
        CheckPointPosition = new float[3];
        CheckPointPosition[0] = player.LastCheckPoint[0];
        CheckPointPosition[1] = player.LastCheckPoint[1];
        CheckPointPosition[2] = player.LastCheckPoint[2];
    }*/
}